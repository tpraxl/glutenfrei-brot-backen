= Glutenfreies Brot backen
:icons: font
:toc-title: Inhaltsangabe
:toc:

== Vorbemerkung

Ich biete am Ende dieses Rezepts eine <<Hintergruende, Liste von Produkten und Herstellern>>.
Das sind einfach die Produkte, die ich verwende oder verwendet habe.

Ich bekomme nichts dafür und will auch keine Werbung dafür machen,
sondern einfach nur Hinweise für die Beschaffung glutenfreier Produkte geben.

Die Beispiele sind mit Sicherheit nicht vollständig.
Schaue Dich also auch nach anderen Herstellern um.

<<<
== Teig-Konsistenz

Glutenfreie Mehle scheinen mir normalerweise häufig trocken und spröde zu sein,
brechen oder reißen also leicht und sind nicht so elastisch.

Ein Wundermittel gegen Trockenheit und für Elastizität ist <<Xanthan, Xanthan>>.
Das ist echt super. Allerdings musst Du selbst ausprobieren ob Du das verträgst.
Manche Menschen mit Zöliakie reagieren empfindlich auf <<Xanthan, Xanthan>>,
trotzdem das glutenfrei ist.

Und Honig scheint den Hefeteig auch vorteilhaft zu verändern.
Das hat wohl mit bestimmten Enzymen zu tun.
Ich habe öfter schon gelesen, man könne wahlweise <<Xanthan, Xanthan>> _oder_ Honig verwenden.
Ich verwende beides und habe damit die besten Erfahrungen gemacht.

Die letzten Brote habe ich zusätzlich mit <<Guarkernmehl, Guarkernmehl>> gebacken.
Das hat ihnen den letzten Schliff gegeben, mehr Elastizität des Brotes.
Ich finde, so sieht es aus wie ein Bäckerbrot und schmeckt auch so.

Demnächst steht noch ein Test mit selbstgemachtem Backmalz an. Dazu später mehr.

== Benötigte Materialien und Voraussetzungen

* Gärkörbchen (alternativ: Kastenform)
* Backblech
* Backpapier
* Ofen für 250°C
* Küchenmaschine mit Knethaken
* ggf. extra Rührschüssel
* Teigschaber
* flaches Schraubglas für die Aufbewahrung des Sauerteigs
* Verschließbarer dehnbarer Behälter (z.B. 1KG Joghurt-Becher aus Plastik mit Deckel)
* ggf. Wärmflasche und Isoliertasche
* ggf. chinesische Stäbchen oder Gitter zum Abkühlen des Brotes

<<<
== Zutaten

* <<Sauerteig, Sauerteig>>
  - 400g <<Teffmehl, Teffmehl>> für den Starter
  - 400ml Wasser

* <<Vorteig, Vorteig>>
  - 200g <<Teffmehl, Teffmehl>>
  - 21g frische Backhefe (halber Würfel)
  - 300ml lauwarmes Wasser

* <<Hauptteig, Hauptteig>>
  - 30g Salz
  - 100g Reismehl
  - 60g Maismehl
  - 60g Buchweizenmehl
  - 30g geschrotete Leinsamen
  - 40g Flohsamenschalen
  - 2TL Xanthan
  - 2TL Guarkernmehl
  - 2TL Honig (zähflüssig)
  - 2EL Olivenöl
  - 400ml lauwarmes Wasser
  - 75–100g Walnusskerne
  - 50–75g Haselnusskerne

* 20–40g Maisstärke zum Bemehlen

<<<
[[Sauerteig]]
== Sauerteig

Aus ca. 1:1 <<Teffmehl, Teffmehl>>, Wasser und dem altem Sauerteig.

Sauerteig setze ich i.d.R. 12 Stunden vor dem Brotbacken an.

=== Erster Sauerteig

Wenn Du einmal einen Sauerteig hast, ist der Umgang damit entspannt.
Der Anfang kann jedoch auch leicht schief gehen. Notfalls kannst Du das Brot übrigens auch ohne Sauerteig backen.

IMPORTANT: Unbedingt ganz genau auf extreme Sauberkeit der Behälter und anderen Materialien achten

TIP: Generell hat sich bei mir bewährt so einen 1KG Joghurt-Plastik-Becher mit Plastik-Deckel zu verwenden.
Die halten schön dicht und haben die richtige Größe. Außerdem ein flaches Schraubglas für später.

In den gut verschließbaren, dehnbaren Behälter kommen:

.Anfangs
* 100g Teffmehl
* 100ml lauwarmes Wasser

Den Behälter gut verschließen. 48 Stunden bei Zimmertemperatur stehen lassen.
Es kann passieren, dass sich ordentlich Druck bildet, den kann man ruhig ablassen.

Das gelingt nicht immer. Es dauert etwas, bis der Teig genügtend Michsäurebakterien enthält, um der Schimmelbildung
etwas entgegenzuhalten.

Um Schimmelbildung zu vermeiden kann man gelegentlich umrühren (z.B. alle 8, 12 oder 24 Stunden), eigentlich
muss das aber nicht sein.

.Nach 48 Stunden
* 100g Teffmehl dazu
* 100ml lauwarmes Wasser dazu

Verschlossen 12 Stunden bei Zimmertemperatur stehen lassen.

.Nach 12 Stunden
* 200g Teffmehl dazu
* 200ml lauwarmes Wasser dazu

Verschlossen 12 Stunden bei Zimmertemperatur stehen lassen.
(Das wird etwas eng in dem Joghurtbecher, öfter mal Druck ablassen).

Die Zeiten können auch überschritten werden. Aber möglichst nicht unterschreiten. Der Teig sollte von
Luftbläschen durchzogen und locker sein.

Jetzt kann der Sauerteig mit anderen Zutaten verbacken werden.

IMPORTANT: Immer 2–3 EL in einem Schraubglas aufbewahren

=== Ansatz Sauerteig mit vorhandenem Starter

Beim nächsten Brot, einfach 12 Stunden vorher 100–200mg Teffmehl und ungefähr die gleiche Menge Wasser
(vielleicht etwas mehr) zusammen mit dem Sauerteig in einen verschlossenen druckresistenten Behälter geben
und ordentlich verrühren. Nach 12 Stunden 2–3 EL davon ins Schraubglas, den Rest ins Brot.

TIP: Den aufgehobenen Sauerteig kann man ruhig 2 Tage im Schraubglas bei Zimmertemperatur aufbewahren.
Ich rühre ihn dann gelegentlich um, wenn ich dran vorbeikomme. So alle 12–24 Stunden. Das muss aber eigentlich nicht sein.
Nach 2 Tagen würde ich ihn schon wieder mit Mehl und Wasser füttern und im Joghurtbecher aufbewahren.

Klingt alles aufwändiger, als es wirklich ist.

<<<
[[Vorteig]]
== Vorteig

* 200g <<Teffmehl, Teffmehl>>
* 20g bzw. 1–2 EL Zucker
* 21g Frischhefe (halbes Päckchen)
* 300ml lauwarmes Wasser

Gut verrühren, z.B. in der Küchenmaschine.

10 Minuten abgedeckt an einem warmen Ort quellen lassen.

TIP: Es hat sich in kalten Jahreszeiten bei mir bewährt, eine Isoliertasche, wie man
sie auch zum Einkaufen nimmt, zu verwenden. Hinein kommt eine Wärmflasche, die halb mit normalkaltem Wasser,
halb mit kochendem Wasser gefüllt ist (Mengen ausprobieren). So schaffen wir günstigere Bedingungen für die Hefe.
Allerdings sollte man aufpassen, dass 40°C nicht überschritten werden, sonst geht die Hefe womöglich kaputt. Ich messe
meist 25–28°C.

TIP: Der Zucker ist zur Unterstützung der Hefe gedacht.

TIP: Falls Du eine Küchenmaschine mit spezieller Schüssel verwendest,
fülle den Vorteig vor dem Quellen in eine andere Schüssel. So kannst Du direkt weitermachen.

<<<
== Hauptteig

.Vorher benötigt:
* <<Sauerteig, Sauerteig>> (Nicht vergessen: unbedingt etwas für das nächste Mal aufheben!)
* <<Vorteig, Vorteig>>

Während der Vorteig 10 – max. 20 Minuten lang quillt, kann der Hauptteig vorbereitet werden.

In die Rührschüssel kommen:

* 30g Salz
* 100g Reismehl
* 60g Maismehl
* 60g Buchweizenmehl
* 30g geschrotete Leinsamen
* 40g Flohsamenschalen
* 2TL Xanthan
* 2TL Guarkernmehl
* 2TL Honig (zähflüssig)

Nachdem vom Sauerteig 2–3 EL im Schraubglas aufbewahrt wurden, wird der Sauerteig hinzugefügt.

Dann:

* 2EL Olivenöl
* 400ml lauwarmes Wasser

Nun kommt der Vorteig obendrauf und Du verrührst alles vorsichtig mit dem Teigschaber.

Dann 10 Minuten bei relativ hoher Geschwindigkeit in der Küchenmaschine mit Knethaken rühren.

TIP: Die 10 Minuten sind wichtig. So wird der Teig schön fluffig. Wenn Du keine Küchenmaschine hast, kannst Du den Teig
auch 20 Minuten mit der Hand kneten. Aber erfahrungsgemäß benötigt man dabei recht viel Mehl, weil es so klebrig ist
und das verändert den Teig negativ.

Nüsse oder Kerne hinzufügen und langsam verrühren, bis sie einigermaßen gleichmäßig verteilt sind:

* 75–100g Walnusskerne
* 50–75g Haselnusskerne

Wenn Du eine Kastenform verwendest, muss die nicht eingefettet werden (kannst Du aber auch machen).
Der Teig kommt direkt hinein.

Besser gelingt das Brot im Gärkörbchen mit Stoffbezug. Den Stoff mehle ich mit ca 15–20g Maisstärke gleichmäßig ein.
Dann mehle ich mir die Hände ordentlich ein, die Arbeitsplatte ein wenig und hole den Teig mit dem Teigschaber aus
der Rührschüssel auf die leicht eingemehlte Arbeitsplatte.

Ich forme mit der Hand den Teig oval vor. Dabei muss ich immer wieder die Hände einmehlen, weil der Teig so klebt.

Dann kommt er in das Gärkörbchen. Dort verteile ich ihn gleichmäßig und presse ihn auch etwas an.

Nun 1 Stunde abgedeckt im Warmen gehen lassen. Wie schon beim Vorteig, empfehle ich in kalten Jahreszeiten die
Verwendung einer Isoliertasche mit Wärmflasche.

Ca 15 Minuten vor Ablauf der Zeit heizt Du den Ofen auf 250°C Ober-/Unterhitze vor.
Lass das Blech mit dem Backpapier noch aus dem Ofen.
Nun stürzt Du das Brot aus dem Gärkörbchen auf das Blech.

Verwendest Du alternativ eine Kastenform, dann stell die Kastenform einfach auf das Backblech.

Nach 20 Minuten senkst Du die Temperatur auf 230°C.

Die Backzeit beträgt insgesamt (je nach Ofen) 50 Minuten (20 Min 250°C + 30 Min 230°C).
Das Mehl darf ruhig an der ein oder anderen Stelle braun werden. Muss nicht, darf aber.

TIP: Das Brot ist fertig, wenn es beim Klopfen hohl klingt.
Das kann man ohne Verbrennungen aber eigentlich erst richtig feststellen, wenn es bereits aus dem Ofen genommen ist.

Dann nimmst Du das Brot aus dem Ofen bzw. stürzt es aus der Kastenform.
Beim Abkühlen achtest Du auf gute Belüftung aller Seiten. Entweder drehst Du das Brot auf die Seite
(das geht nur bei Kastenformen) oder Du legst es z.B. auf chinesische Essstäbchen oder ein Gitter.

Manchmal klopfe ich aus Neugier mal hier und da und wende es und rede dem Brot gut zu,
während ich mich auf das Anschneiden freue. Aber ich glaube, das muss nicht unbedingt sein.

Warte ein bisschen mit dem Aufschneiden, es muss noch etwas Festigkeit gewinnen.

Dann: Guten Appetit :)

<<<
[[Hintergruende]]
== Hintergründe zu den Zutaten

Ich schreibe einfach, soweit ich weiß, Marke und Herkunft dazu. Die sind dann glutenfrei.

[[Xanthan]]
=== Xanthan

Z.B. Natura Xanthan (von Naturawerk) aus dem Reformhaus.

Bindemittel. Macht den Teig elastisch und hält ihn feucht. Scheinbar ist die Kombination mit Johannisbrotkernmehl
sehr gut für Elastizität. Ich kann das weder widerlegen noch bestätigen. Ich habe eine Zeitlang mit Johannisbrotkernmehl
kombiniert, mache das in letzter Zeit aber nicht mehr.

https://de.wikipedia.org/wiki/Xanthan

[[Guarkernmehl]]
=== Guarkernmehl

Z.B. Natura Guarkernmehl (von Naturawerk) aus dem Reformhaus.

Bindemittel. Hat in meinen Versuchen in Verbindung mit Xanthan zur perfekten Elastizität und Feuchtigkeit geführt.

[[Flohsamenschalen]]
=== Flohsamenschalen

Z.B. von borchers aus dem Rewe.

Quellen, machen satt und sind gut gegen Verstopfung und für den Darm.

[[Leinsamen]]
=== Leinsamen

Z.B. Linusit oder Linufit pur aus dem Reformhaus.

Proteinbomben. Machen satt und sind lecker.

[[Nuesse-und-Kerne]]
=== Sonnenblumenkerne, Walnüsse und Kürbiskerne

Z.B. von Maryland aus dem Rewe.

Lecker & gesund. Insbesondere die Walnusskerne halten das Brot schön feucht.

[[MixIt]]
=== Mix It Mehl

Von Schär aus dem Rewe.

* Universal
* Rustico

In diesen Mix-Mehlen sind oft auch schon Zucker oder Bindemittel enthalten.
Das finde ich persönlich hinderlich beim Kennenlernen der Eigenschaften verwendeter Zutaten.

Andererseits gelingt mir z.B. Pfannkuchenteig immer noch am Besten mit Schär MixIt Universal. Eigentlich muss ich sogar
sagen, dass er bisher eine Katastrophe war, wenn ich mit Buchweizen, Reis- und Maismehl und Xanthan darangegangen bin.

Tatsächlich waren die Schär Mehle bisher schon in vielen unserer Brote, Pizzen, Crêpes, Pfannkuchen und Kuchen enthalten.
Wir haben damit gute Ergebnisse erzielt und wir haben schon Unmengen davon verarbeitet.

Mix It Rustico funktioniert z.B. gut in dunklen Teigen (macht Pizzateig aber z.B. zu gummiartig).
Es ist eher ungünstig in <<Vorteig, Vorteigen>>, weil es gerne so eine Art Gummi-Klumpen bildet
(das kommt vermutlich durch das bereits enthaltene <<Xanthan, Xanthan>>).

[[Teffmehl]]
=== Teffmehl

Z.B. von borchers aus dem Rewe.

Teffmehl ist großartig. Teff ist ein afrikanisches Gras. Lecker.
Eignet sich perfekt für die Zubereitung von Sauerteig und dunklen Teigen.

https://de.wikipedia.org/wiki/Teff

[[Mehle]]
=== Buchweizen-, Reis- und Maismehl

* von Hof Bauck aus dem Tegut oder Reformhaus
* Buchweizenmehl von Spiegelhauer im 4Kg Pack aus dem Online-Handel (immer drauf achten, dass es glutenfrei ist!)
* Reformhaus Reis- und Maismehl von Reformkontor aus dem Reformhaus

Menge und Art variieren für Änderungen im Geschmack und der Konsistenz. Es gibt übrigens auch glutenfreie Hanfmehle im Onlinehandel,
das kam bei meiner Familie aber nicht so gut an. Kastanienmehl, Kichererbsenmehl, … einfach mutig experimentieren
(und gerne Ergebnisse zurückmelden).
