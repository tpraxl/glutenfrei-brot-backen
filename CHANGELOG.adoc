= Change Log

Alle wichtigen Änderungen an diesem Projekt werden hier dokumentiert.
Dieses Projekt richtet sich nach http://semver.org/[Semantischer Versionierung].

== [0.1.1] – 2020-01-09

Code: Make Target für CI Test Build korrigiert.

== [0.1.0] – 2019-12-31

Initiales Release
